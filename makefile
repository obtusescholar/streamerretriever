SRC = streamerretriever
FILES = ${SRC} CHANGELOG.md LICENSE README.md makefile 
VER = $$(sed -n "/__version__/p" ${SRC}/constants.py | cut -d"'" -f2)
PYTHON = python3

.PHONY = clean-pyc clean-build test run version tarball

clean-pyc:
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	rm --force --recursive ${SRC}/__pycache__
	@printf "\n"

clean-build:
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info
	@printf "\n"

test:
	# pipenv check
	@printf "\n"
	${PYTHON} -m flake8 --exclude="ui_*" ${SRC}

build: clean-build test
	@printf "\n"
	${PYTHON} setup.py sdist bdist_wheel
	@printf "\nPyPI\n$(PYTHON) -m twine upload dist/*"
	@printf "\nPyPI Test\n$(PYTHON) -m twine upload --repository testpypi dist/*"

run:
	@${PYTHON} -m ${SRC}.cli -d

version:
	@echo ${VER}

tarball: clean-pyc test
	@printf "\n"
	tar -czvf bin/tar/${SRC}-${VER}.tar.gz ${FILES}
