### Ver 0.20

##### 0.20.4
- Bugfix: made OAuth2 state value accessible for GUI

##### 0.20.3
- Bugfix: Regex issue with twitch token URL - Issue #4
- Bugfix: Readme program name inconsistency - Issue #2, #3

##### 0.20.2
- Bugfix: shows all twitch follows even if more than 20

##### 0.20.1
- Windows configs into ./configs
- Token regex into function

##### 0.20.0
- Rewritten for Twitch Helix API
- Allows viewing VODs
- Print follows from Twitch


### Ver 0.9.0
- Start chat

### Ver 0.8.7
- Security update regex check link before play

### Ver 0.8.6
- Security update
  - urllib.request url checked with regex

### Ver 0.8.5
- getKeys(pretty=True) method

### Ver 0.8.1
- getDb() method

### Ver 0.8.0
- Play method redone
  - Pid is not supported with default browser
- Makefile

### Ver 0.7.9
- Method showing how long stream has been online

### Ver 0.7.6
- Added support for returning pids
